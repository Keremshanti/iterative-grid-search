%% Full Grid Search for RSS-Based Multiple Emitter Localization
% This script implements the Full Grid search by minimising the sum of
% squared differences between the received power and the estimated received
% power calculated using known emitter power. Detailed information in
% provided under the "Emitter Location Estimation" part.
%
% <html>
% <table border=1>
% <tr><td>Name</td>       <td>Description</td></tr>
% <tr><td>ROI</td>        <td>Area of Interest</td></tr>
% <tr><td>gridSize</td>   <td>Grid Size</td></tr>
% <tr><td>N_s</td>        <td>Number of Sensors</td></tr>
% <tr><td>N_e</td>        <td>Number of Emitters</td></tr>
% <tr><td>P_T</td>        <td>Real Emitter Transmit Powers</td></tr>
% <tr><td>alpha</td>        <td>Path Loss Exponent</td></tr>
% <tr><td>sigma</td>      <td>Shadow Spread Variance</td></tr>
% <tr><td>P_E</td>        <td>Assumed Emitter Transmit Powers</td></tr>
% </table>
% </html>

function [] = FGS_ME()

clear all;
close all;
clc;

Main();

end

function [] = Main()
%% Assign Simulation Parameters
% Here the output and main simulation parameters are assigned.

% Output flags

% Plot Placements
plotON = 1;
% Display Placements/Estimation
dispON = 1;
% Time the Script
useTime = 0;
% Assign sensor locations
assignS = 0;
% Assign emitter locations
assignE = 0;
% Place in center
pin = 0;

% Main simulation parameters
disp('Full Grid Simulation Multiple Emitter');
% Region of Interest size from 0 to ROI-1
ROI = 4;
disp(['Region of Interest is: ', num2str(ROI), 'x',  num2str(ROI)]);
% Grid Size
gridSize = 1;
disp(['Region is divided into grids of size: '...
        , num2str(gridSize), 'x',  num2str(gridSize)]);    
% Number of sensors and emitters
N_s = 5;
N_e = 2;
disp(['Number of sensors: ', num2str(N_s)]);
disp(['Number of emitters: ', num2str(N_e)]);
% Path Loss Exponent (>0)
alpha = 3.5;
disp(['Path Loss Exponent: ', num2str(alpha)]);
% Shadow spread (dB)
sigma = 0;
disp(['Shadow Spread (dB): ', num2str(sigma)]);
% Real Transmit powers of the emitters
P_T = [1, 2, 3];
for i=1:N_e
    disp(['Real Transmit Power of Emitter '...
            ,num2str(i),' is: ', num2str(P_T(i))]);
end
% Assumed Transmit powers of the emitters
P_E = [1, 2, 3];
for i=1:N_e
    disp(['Assumed Transmit Power of Emitter '...
            ,num2str(i),' is: ', num2str(P_E(i))]);
end

% Start a timer to observe how much time it takes until the of the
% simulation.
if(useTime)
    startTime = tic;
end

% Here we create the 2D coordinate system as two matrices each including
% their associated indexes. This plane will be our Area of Interest (ROI).
% ROI is divided into grids to minimize computation.
%
% Vectorization in MATLAB is important as reallocation of memory takes a
% lot of time in simulations therefore everything should be predefined for
% best performance.

u = 0:1:ROI-1;                              % Locations on x axis
v = 0:1:ROI-1;                              % Locations on y axis
[x,y] = meshgrid(u, v);                     % 2D plane indices for x and y

newROI = ceil(ROI/gridSize);                % ROI size with grid
xGrid = zeros(newROI ,newROI );
yGrid = zeros(newROI ,newROI );

for k=0:gridSize:ROI-1
    xGrid(:,(k/gridSize+1)) ...
        = x(1:newROI , k+1)+gridSize/2;
    yGrid((k/gridSize+1),:) ...
        = y(k+1,1:newROI)+gridSize/2;
end

%% Initilizing Sensor/Emitter Locations and Creating the Distance Matrix
% Making use of the vectorized coordinate system we will put the sensors
% and the emitter in the simulation and create the distance matrices.
% Sensors and the emitter is placed randomly. They are selected so that
% none of them are on the same coordinates.
%
% The number of sensors and emitters are defined here. To change the number
% of sensors one should change N_s and to change the number of emitters one
% should change N_e. Sensor and emitter locations are assigned randomly.
% They are ensured to never be on the same coordinates.

if assignS
    sPos = assignS;
 
    if (dispON>0)
        for i=1:N_s        
            disp(['Sensor '...              % Display selected coordinates
                    , num2str(i), ' is located at (', num2str(sPos(1,i))...
                    ,',', num2str(sPos(2,i)), ').']);
        end
    end
else    
    % Sensor positions on (x,y) plane
    sPos = zeros(2,N_s);
    for i=1:N_s
        isDistinct = 0;                     % Boolean flag
        while ~isDistinct
            if(pin)
                sPos(:,i) = [randi([0 ROI-1], 1, 1)...
                                ; randi([0 ROI-1], 1, 1)];
            else
                sPos(:,i) = [rand()*ROI; rand()*ROI];
            end
            isDistinct = 1;                 % True unless proven otherwise
            for j=1:i-1
                if (abs(sPos(1,i)-sPos(1,j))<gridSize ...
                    && abs(sPos(2,i)-sPos(2,j))<gridSize)
                    isDistinct = 0;         % Shown that it is not distinct
                end
            end
        end
        if (dispON>0)
            disp(['Sensor '...              % Display selected coordinates
                    , num2str(i), ' is located at (', num2str(sPos(1,i))...
                    ,',', num2str(sPos(2,i)), ').']);
        end
    end
end
if assignE
    ePos = assignE;
    if (dispON>0)
        for k=1:N_e        
            disp(['Emitter '...              % Display selected coordinates
                    , num2str(k), ' is located at (', num2str(ePos(1,k))...
                    ,',', num2str(ePos(2,k)), ').']);
        end
    end    
else    
    % Emitter position on (x,y) plane
    ePos = zeros(2,1);
    for t=1:N_e
       % Boolean flag 
       isDistinct = 0;
        while ~isDistinct
            if(pin)
                ePos(:,t) = [randi([0 ROI-1], 1, 1)...
                                ; randi([0 ROI-1], 1, 1)];
            else
                ePos(:,t) = [rand()*ROI; rand()*ROI];
            end
            % True unless proven otherwise
            isDistinct = 1;
            for k=1:N_s
                if (abs(sPos(1,k)-ePos(1,t))<gridSize ...
                    && abs(sPos(2,k)-ePos(2,t))<gridSize)
                    % Shown that it is not distinct
                    isDistinct = 0;
                end
            end
            for k=1:t-1
                if (abs(ePos(1,t)-ePos(1,k))<gridSize ...
                    && abs(ePos(2,t)-ePos(2,k))<gridSize)
                    % Shown that it is not distinct
                    isDistinct = 0;
                end
            end
        end
        if (dispON>0)
            disp(['Emitter '...
                    , num2str(t), ' is located at (', num2str(ePos(1,t))...
                    ,',', num2str(ePos(2,t)), ').']);
        end
    end
end

% Distance from Sensor matrices
sDist = zeros(ROI, ROI, N_s);
for i=1:N_s    
    for j=1:N_e
        sDist(:,:,i) = sqrt((sPos(1,i)-xGrid).^2 + (sPos(2,i)-yGrid).^2);
    end
end

%% Creating Received Signals
% These variables are required to create the signal model 
% $m_i = P_T d_i^{- \alpha}$ in Watts where 
% $1 \geq i \geq N_s$, $$P_T$ is the the transmit power of the emitter, 
% $\alpha$ is the path loss exponent and 
% $d_i=\sqrt{(x_i-x_0)^2 + (y_i-y_0)^2}$ is the distance between the
% transmitter and the ith sensor.
%
% The sensor's experience log-normal shadowing. If the fast fading effects
% are sufficiently averaged over time then the resulting unknown measured
% power from the emitter to the ith sensor is given as 
% $r_i = 10^{log_{10} (m_i) + \frac{\omega_i}{10}}m_i \omega_i$ where 
% $\omega_i$ is a normal random variable with mean of 0 and a variance of 
% $\sigma^2$. Its PDF is 
% $\frac{1}{\sqrt{2 \pi \sigma^2}} e^{-\frac{(r_i-m_i)^2}{2 \sigma^2}}$

% Distance from sensor to emitter
d = zeros(1,N_s,N_e);
% Noise free signal in Watts
m = zeros(1,N_s);
% Calculate both for each emitter
for i=1:N_e
    d(:,:,i) = ...                      
    sqrt((sPos(1,:)-ePos(1,i)).^2 + (sPos(2,:)-ePos(2,i)).^2);
    m = m + P_T(i) * d(:,:,i).^(-alpha);  
end

r = m;
for j=1:N_s
    r(i) = 10^(log10(m(i)) + normrnd(0,sigma)/10);
end

% Before we start emitter estimation, time passed until now is displayed.
% A new timer is started to measure the time passed during the estimation.
if(useTime)
    disp(['Time passed until the start of the estimation is '...
        , num2str(toc(startTime))]);
    simTime = tic();
end

%% Emitter Location Estimation
% We need to use a cost function so that the coordinates that minimise the 
% cost function is the most likely coordinates for emitter to be in. The
% proposed method here is to use a cost function such as 
% $\sum_{i = 1}^{N_s} (r_i- \sum_{j=1}^{N_e} P_{T_j} d_{i,j}^{-\alpha})^2$.
% This approach minimizes the sum of squared differences between the 
% received power and the estimated received power calculated using known
% emitter power difference. Estimated received power from ith sensor is
% $P_{R_i} = P_E d_i^{\alpha}$ where $P_E$ is the emitter transmit power, 
% $d_i$ is the distance of the emitor to the ith sensor and $\alpha$ is the 
% path loss exponent. 

eComb = nchoosek(1:ROI*ROI,N_e);        % Emitter combination matrix
estPESum = 0;                           % Estimated Emitter Power Sum
theta = ones(1, 1)*Inf;                 % Sum of (Received - Estimated) 
tempTheta = 0;                          % Temporary theta value
minThetaIndex = zeros(1, 2);            % Min theta's indices (n, p)
coord = [0 0];                          % Converted current coordinates
tCount = 0;                             % Number of minimum thetas

% Store objective function values for each combination
thetaTable = zeros(length(eComb),(N_e*2+1)*2);

for n=1:length(eComb)
    ePerm = perms(eComb(n,:));
    for p=1:length(ePerm)
        % Find nth theta value (nth combination of emitter location)
        tempTheta = 0;
        for i=1:N_s        
            estPESum = 0;        
            for j=1:N_e
                coord = l22d(ePerm(p,j),ROI);
                estPESum = estPESum ...
                    + P_E(j)*sDist(coord(2),coord(1),i).^(-alpha);
            end
            %tempTheta = tempTheta + (log10(r(:,i)) - log10(estPESum))^2;
            tempTheta = tempTheta + (r(:,i) - estPESum)^2;
        end
        
        % Put theta value into table with their related coordinates. Format
        % is provided below where e1x is x coordinate of 1st emitter and so
        % on;
        % Theta e1x e1y e2x e2y Theta e1x e1y e2x e2y
        % In each line permutations of placement are given 
        % (left e1x = right e2x)
        if (p==1)
            thetaTable(n,1) = tempTheta;
            % Theta coordinates
            coords=l22d(ePerm(p,1),ROI);
            thetaTable(n,4) = coords(1)-1;
            thetaTable(n,5) = coords(2)-1;  
            coords=l22d(ePerm(p,2),ROI);
            thetaTable(n,2) = coords(1)-1;
            thetaTable(n,3) = coords(2)-1;
        end
        if (p==2)
            thetaTable(n,6) = tempTheta;
            % Theta coordinates
            coords=l22d(ePerm(p,1),ROI);
            thetaTable(n,9) = coords(1)-1;
            thetaTable(n,10) = coords(2)-1;  
            coords=l22d(ePerm(p,2),ROI);
            thetaTable(n,7) = coords(1)-1;
            thetaTable(n,8) = coords(2)-1;
        end        
        
        % If nth theta is smaller than smallest theta store it in 1st index
        if(tempTheta==theta)
            tCount = tCount+1;
            minThetaIndex(tCount,1) = n;
            minThetaIndex(tCount,2) = p;
        end
        if(tempTheta<theta)
            theta = tempTheta;
            minThetaIndex(1,1) = n;
            minThetaIndex(1,2) = p;
            tCount = 1;
        end        
    end
end

assignin('base','thetaTable',thetaTable);
assignin('base','theta',theta);

disp(['Minimum found objective function value is ', num2str(theta)]);

% Now that the estimation is over how much time passed is displayed.
if(useTime)
    disp(['Time passed during the estimation is '...
        , num2str(toc(simTime))]);
end
% Now we have the correct combination we need to find the related emitter
% locations. There might be multiple combinations especially if they emit
% same amount of power.

estELoc = zeros(2, N_e, tCount);        % Found Emitter Locations
cc = zeros(tCount,N_e);                 % Correct combination  
for i=1:tCount
    cc(i,:) = eComb(minThetaIndex(i,1),:);    
    cp = perms(eComb(...                % Correct permutation
        minThetaIndex(i,1),:));
    cp = cp(minThetaIndex(i,2),:);
    for j=1:N_e    
        currE=l22d(cp(j),ROI);          % Current Emitter
        % Area is 0 to ROI-1 so index-1 gives the coordinate
        % gridSize/2 is added as calculations are done in the center of the
        % grids
        estELoc(1, j, i) = currE(1)-1+gridSize/2;
        estELoc(2, j, i) = currE(2)-1+gridSize/2;                
    end
    if(i~=tCount)
        disp('         or');
    end
end

disp(' ');

% Now we the possible locations we can find the errors from possible
% locations. Smallest one gives the right combination.

% Estimated Error
estErr = zeros(1,N_e,tCount);

for i=1:tCount
    for j=1:N_e       
        estErr(1,j,i) = sqrt((ePos(1,j)-(estELoc(1,j,i))).^2 ...
            + (ePos(2,j)-(estELoc(2,j,i))).^2);
        disp(['Emitter ', num2str(j) , ' is found in ('...
                ,num2str((estELoc(1,j,i))),','...
                ,num2str(estELoc(2,j,i)), ').']);
        disp(['Estimation error for Emitter ', num2str(j)...
                , ' is ', num2str(estErr(1,j,i))  , ' meters.'])                
    end
    if(i~=tCount)
        disp('         or');
    end
end

% Total time it took to run this script is displayed
if(useTime)
    disp(['Total time passed during the execution of this script is '...
        , num2str(toc(startTime))]);
end

%% Plot Sensor/Emitter Placement and Estimations
if(plotON)
    plotSELocations(ROI, gridSize, N_e, sPos, ePos, estELoc)
end

end

%% Inner functions
% After here inner functions used in code are provided.

%% Change index from linear to 2D
% This function takes the linear index of a square matrix and converts it
% to (x,y) format.
%
% Ex: For a 4x4 matrix 15th index is (4,3)
%     For a 3x3 matrix 3rd index is (1,3)

function [ coordinates ] = l22d( index , ROI )

    y = mod(index,ROI);
    x = (index-mod(index,ROI))/ROI + 1;
    if(y==0)
        y = ROI;
        x = x-1;
    end  
    coordinates = [x,y];
end

%% Change index from linear to 2D
% This function takes the linear index of a square matrix and converts it
% to (x,y) format.
%
% Ex: For a 4x4 matrix 15th index is (4,3)
%     For a 3x3 matrix 3rd index is (1,3)

function [ index ] = from2d2l( x, y , AOI )

    if(y==AOI)
        y = 0;
        x = x+1;
    end
    
    index = (x-1)*AOI+y;
   
end


function [] = plotSELocations(ROI, gridSize, N_e, sPos, ePos, estELoc)
%% PLOTSELOCATIONS plots sensors and emittors on figure to show locations
% Required inputs are;
% - Region of interest axis length (ROI)
% - Grid axis length (gridSize)
% - Sensor positions given as [x;y] (sPos)
% - Emitter positions given as [x;y] (ePos)

% Create whole area
axis([0 ROI+1 0 ROI+1]);
hold on;
% Place actual sensors
scatter(sPos(1,:),sPos(2,:), 70, 'x');
% Place actual emitters also determine color for each
c = zeros(N_e,3);
for k=1:N_e
    c(k,:)= [round(rand), round(rand), round(rand)];
    while isequal(c(k,:),[1 1 1])
        c(k,:)= [round(rand), round(rand), round(rand)];
    end
end

for k=1:N_e
    scatter(ePos(1,k),ePos(2,k), 70, 's' ...
                , 'MarkerFaceColor', c(k,:) ...
                , 'MarkerEdgeColor', c(k,:));
end

% Place estimated emitter locations
for k=1:length(estELoc(1,1,:))
    for l=1:length(estELoc(1,:,k))
        scatter(estELoc(1,l,k), estELoc(2,l,k), 70, 'd'...
                    , 'MarkerFaceColor', c(l,:)...
                    , 'MarkerEdgeColor', c(l,:));
    end
end
hold off;
% Set grid on with the given size
tickValues = 0:gridSize:ROI+1;
set(gca,'YTick',tickValues)
set(gca,'XTick',tickValues)
grid on;

% Set title, labels and legend
title('Sensor and Emitter placements for FGS_{ME}');
xlabel('x');
ylabel('y');
legend('Sensors', 'Emitter 1', 'Emitter 2' ...
        , 'Estimated Emitter 1', 'Estimated Emitter 2');

end