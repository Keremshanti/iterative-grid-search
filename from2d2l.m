%% Change index from linear to 2D
% This function takes the linear index of a square matrix and converts it
% to (x,y) format.
%
% Ex: For a 4x4 matrix 15th index is (4,3)
%     For a 3x3 matrix 3rd index is (1,3)

function [ index ] = from2d2l( x, y , AOI )

    if(y==AOI)
        y = 0;
        x = x+1;
    end
    
    index = (x-1)*AOI+y;
   
end