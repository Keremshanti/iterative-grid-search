function [ distance ] = distanceInGrid( gridSize )
%% Distance In Grid
% This function simulates placement of a emitter in a grid with size given 
% as input and returns its distance from the center.

% Emitter Position
ePos = zeros(2,1);

% Randomly place Emitter to a coordinate
ePos(:,1) = [randi([0 gridSize-1], 1, 1); randi([0 gridSize-1], 1, 1)];

% % Display where it is placed
% disp(['Emitter '...              % Display selected coordinates
%         , ' is located at (', num2str(ePos(1,1))...
%         ,',', num2str(ePos(2,1)), ').']);

distance = sqrt( (gridSize/2 - ePos(1,1))^2 + (gridSize/2 - ePos(2,1))^2 );

end

