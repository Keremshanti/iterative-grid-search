%% Create Histogram
% Creates normalized histogram from the distance values provided by 
% distanceInGrid function.

close all;
clc;

trialSize = 100000;
gridSize = 10;
data = zeros(1,1000);

for k = 1:trialSize
    data(k) = distanceInGrid(gridSize);
    %data(k) = randi([0 gridSize-1], 1, 1);
end

histnorm(data);
title(['Distance from center to actual location in grid for grid size '...
        , num2str(gridSize)]);
xlabel('X');
ylabel('P(X)');
