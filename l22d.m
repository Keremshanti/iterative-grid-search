%% Change index from linear to 2D
% This function takes the linear index of a square matrix and converts it
% to (x,y) format.
%
% Ex: For a 4x4 matrix 15th index is (4,3)
%     For a 3x3 matrix 3rd index is (1,3)

function [ coordinates ] = l22d( index , ROI )

    y = mod(index,ROI);
    x = (index-mod(index,ROI))/ROI + 1;
    if(y==0)
        y = ROI;
        x = x-1;
    end  
    coordinates = [x,y];
end