# README #

Main functions implement the Full and Iterative Grid search method provided in the article "Iterative Grid Search for RSS-Based Emitter Localization". The variables required as input to the function is described below. Detailed  descriptions of these variables are given in the ".html" documentation found in the repository.

Figures are provided to;
- Illustrate Objective function calculated in a single FGS
- Plot outcome of several Monte Carlo runs with different parameters changing

In figures;
* FGS is Full Grid Search
* IGS is Iterative Grid Search
* KE is Known Emitter
* SS is Shadow Spread
* MEE is Mean Estimation Error
* GS is Grid Size
* "Linear" and "dB" is denoted to show how the objective function is calculated