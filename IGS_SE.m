%% Iterative Grid Search for RSS-Based Emitter Localization
% Main function implements the Iterative Grid search method provided in the 
% article given in the title. The variables required as input to the
% function is described below. Detailed descriptions of these variables are
% given in under the "Main Function" section.
%
% <html>
% <table border=1>
% <tr><td>Name</td>       <td>Description</td></tr>
% <tr><td>ROI</td>        <td>Region of Interest</td></tr>
% <tr><td>gridSize</td>   <td>Grid Size</td></tr>
% <tr><td>N_s</td>        <td>Number of Sensors</td></tr>
% <tr><td>P_T</td>        <td>Real Emitter Transmit Power</td></tr>
% <tr><td>sigma</td>      <td>Shadow Spread</td></tr>
% <tr><td>alpha</td>      <td>Path Loss Exponent</td></tr>
% <tr><td>plotON</td>     <td>Plot Zoom Level</td></tr>
% <tr><td>dispON</td>     <td>Display placement and estimation</td></tr>
% <tr><td>assignS</td>    <td>Assign sensors, 0 otherwise</td></tr>
% <tr><td>assignE</td>    <td>Assign emitter, 0 otherwise</td></tr>
% </table>
% </html>
%
% Dependencies for Plots:
% makedatatip.m
% subtightplot.m

%% Start Function
% This is the first function that is run when executed. Uncomment the
% relevant function to work with.
function [] = IGS_SE()

clear all;
close all;
clc;

%Test();
%findLargeErr(20);
FGS_MEEvsSS_VaryingGS()
%IGS_MEEvsSS();
%IGS_MEEvsNofS();

end

%% Example Test Function
% This function initilizes the variables for the main function.
function [] = Test()

    ROI = 10;                               % Area of Interest
    gridSize = 1;                           % Grid Element Size
    N_s = 4;                                % Number of Sensors
    P_T = 1;                                % Transmit Power of the Emitter
    P_E = 1;                                % Assumed Transmit Power
    sigma = 0;                              % Shadow Spread (Watts)
    alpha = 3.5;                            % Path Loss Exponent    
    dispON = 1;                             % Display Placements/Estimation    
    useKE = 0;                              % Calculate Cost Function with
                                            % Known Emitter Power Values
    useTime = 0;                            % Time the Script
    maxLevel = 0;                           % Maximum Level
    is4by4 = 0;                             % Iterative for 2x2 or 4x4 flag
    

    % TEST Purposes
    sPos(:,1) = [7.195;8.1725];
    sPos(:,2) = [3.025;8.9505];    
    sPos(:,3) = [8.3789;4.5527];
    sPos(:,4) = [0.82942;7.4409];    
    for k=5:N_s
        sPos(:,k) = [rand()*ROI;rand()*ROI];
    end    
    assignS = sPos;                  % Assign Sensors, 0 otherwise
    % Assign Emitter Locations
    ePos(:,1) = [7.3978;9.977];
    assignE = ePos;                  % Assign Sensors, 0 otherwise

    estErr = Main( ROI, gridSize, N_s, P_T, P_E, sigma, alpha...
            , dispON...
            , useTime, useKE...
            , assignS, assignE...
            , maxLevel, is4by4);
end

function [] = findLargeErr(threshold)   
    ROI = 1000;                             % Area of Interest
    gridSize = 2;                           % Grid Element Size
    N_s = 4;                                % Number of Sensors
    P_T = 1;                                % Transmit Power of the Emitter
    P_E = 1;                                % Assumed Transmit Power
    sigma = 0;                              % Shadow Spread (Watts)
    alpha = 3.5;                            % Path Loss Exponent
    dispON = 0;                             % Display Placements/Estimation    
    useKE = 0;                              % Calculate Cost Function with
                                            % Known Emitter Power Values
    useTime = 0;                            % Time the Script
    

    % Assign Sensor Locations
    sPos(:,1) = [0;957];
    sPos(:,2) = [519;64];
    sPos(:,3) = [134;705];
    sPos(:,4) = [967;137];
    assignS = 0;                  % Assign Sensors, 0 otherwise
    % Assign Emitter Locations
    ePos(:,1) = [126;688];
    assignE = 0;                  % Assign Sensors, 0 otherwise
    
    estErr = 0;
    while(estErr<threshold)
        [estErr, sPos, ePos] ...
            = Main( ROI, gridSize, N_s, P_T, P_E, sigma, alpha...
            , dispON...
            , useTime, useKE...
            , assignS, assignE);
    end
    disp(['Estimated error is ' num2str(estErr)...
         ' for the sensor/emitter placement;'])
    disp(['sPos(:,1) = [' num2str(sPos(1,1)) ';' num2str(sPos(2,1)) '];']);
    disp(['sPos(:,2) = [' num2str(sPos(1,2)) ';' num2str(sPos(2,2)) '];']);
    disp(['sPos(:,3) = [' num2str(sPos(1,3)) ';' num2str(sPos(2,3)) '];']);
    disp(['sPos(:,4) = [' num2str(sPos(1,4)) ';' num2str(sPos(2,4)) '];']);
    disp('assignS = sPos;                  % Assign Sensors, 0 otherwise')
    disp('% Assign Emitter Locations')
    disp(['ePos(:,1) = [' num2str(ePos(1,1)) ';' num2str(ePos(2,1)) '];']);
    disp('assignE = ePos;                  % Assign Sensors, 0 otherwise')
    
end

%% Mean Estimation Error vs Shadow Spread
% Following two functions creates the figure 3 and 4 in the title making use of
% the "Main" function.

%% Simulation For Changing Grid Size
%
% Here we call the function for 6 different area of interest sizes and 12
% different shadow spread values trialSize times and find the estimated 
% error for each. This creates a large 3D estimated error matrix.
% Area of interest is AOIxAOI
% Grid size varies
% Number of sensors is N_s
% Shadow spread varies
% Path loss exponent is alpha
function [] = FGS_MEEvsSS_VaryingGS()
%%
% Below we initialize the figure parameters. X-axis is shadow spread and
% Y-axis is the mean estimation error. Number of calculations before taking
% the estimated value for mean error is trialSize and Y-axis starts from 0
% and continues until maxShadowSpread. The area of interest size is denoted
% as GS_s.

ROI = 1000;                                 % Area of Interest
N_s = 50;                                   % Number of Sensors
trialSize = 1000;                           % Number of Function Calls
P_T = 1;                                    % Transmit Power of the Emitter
P_E = 1;                                    % Assumed Transmit Power
alpha = 3.5;                                % Path Loss Exponent
dispON = 0;                                 % Display Placements/Estimation
useKE = 0;                                  % Calculate Cost Function with
                                            % Known Emitter Power Values
maxLevel = 0;                               % Maximum Level (FGS=0)
is4by4 = 0;                                 % Iterative for 2x2 or 4x4 flag
useTime = 0;                                % Time the Script
assignS = 0;                                % Assign Sensors, 0 otherwise
assignE = 0;                                % Assign Emitter, 0 otherwise

maxShadowSpread = 12;                       % Maximum Shadow Spread
GS_s = zeros(6,1);                          % Grid Sizes
GS_s(1) = 10;
GS_s(2) = 20;
GS_s(3) = 25;
GS_s(4) = 40;
GS_s(5) = 50;
GS_s(6) = 100;

disp(['Starting ''FGS Mean Estimation Error vs Shadow Spread Simulation ' ...
        'For Changing Grid Size']);
disp('using IGS code with maxLevel=0');

estErr = zeros(trialSize,maxShadowSpread,6);
meanEstErr = zeros(1,maxShadowSpread+1,6);
for GS=1:6
    for SS=0:maxShadowSpread
        for i=1:trialSize            
            estErr(i,SS+1,GS)...                
            = Main( ROI, GS_s(GS), N_s, P_T, P_E, SS, alpha...
                    , dispON...
                    , useTime, useKE...
                    , assignS, assignE...
                    , maxLevel, is4by4);                       
        end
        disp(['Shadow Spread ', num2str(SS) , ' is complete.']);
    end
    disp(['Grid Size ', num2str(GS_s(GS)), ...
        'x', num2str(GS_s(GS)) , ' is complete.']);
end

%%
% Calculation of mean estimated error values for different shadow spread.
for GS=1:6
    for SS = 0:maxShadowSpread
        disp(['Mean estimated error for shadow spread '...
                , num2str(SS), ' is ' ...
                , num2str(sum(estErr(:,SS+1,GS))/trialSize)]);        
        meanEstErr(1,SS+1,GS) = sum(estErr(:,SS+1,GS))/trialSize;
    end
end

%%
% Now we need to visualize the estimated error data for different area of
% interest and shadow spread values. We will plot mean estimation error
% versus shadow spread and plot this for different area of interest values 
% on top of each other.

if(useKE)
    figure('name','FGS Known Emitter','numbertitle','off');
else
    figure('name','FGS Unknown Emitter','numbertitle','off');
end

plot(0:maxShadowSpread, meanEstErr(1,:,1), 'x-'...
    ,0:maxShadowSpread, meanEstErr(1,:,2), '.-'...
    ,0:maxShadowSpread, meanEstErr(1,:,3), '*-'...
    ,0:maxShadowSpread, meanEstErr(1,:,4), 'h-'...
    ,0:maxShadowSpread, meanEstErr(1,:,5), 'v-'...
    ,0:maxShadowSpread, meanEstErr(1,:,6), 'p-');
grid on;
title({...
    'Mean Estimation Error for Different Shadow Spread and Grid Sizes' ...
    , strcat('ROI = ', num2str(ROI), ', N_s = ', num2str(N_s) ...
    , ', trialSize = ', num2str(trialSize), ', P_T = ', num2str(P_T) ...
    , ', P_E = ', num2str(P_E), ', alpha = ', num2str(alpha) ...
    , ', useKE = ', num2str(useKE))});
xlabel('Shadow spread (dB)');
ylabel('Mean Estimation error (m)');
hLegend = legend('10x10'...
                ,'20x20'...
                ,'25x25'...
                ,'40x40'...
                ,'50x50'...
                ,'100x100');
v = get(hLegend, 'title');            
set(v,'string', 'Grid Sizes');

end

function [] = IGS_MEEvsSS()
%%
% Below we initialize the figure parameters. X-axis is shadow spread and
% Y-axis is the mean estimation error. Number of calculations before taking
% the estimated value for mean error is trialSize and X-axis starts from 0
% and continues until maxShadowSpread. The grid sizes are denoted as GS_s.


%%
% Initilization of variables
ROI = 1000;                                 % Area of Interest
N_s = 50;                                   % Number of Sensors
gridSize = 10;                              % Grid Element Size
trialSize = 1000;                           % Number of Function Calls
P_T = 1;                                    % Transmit Power of the Emitter
P_E = 1;                                    % Assumed Transmit Power
alpha = 3.5;                                % Path Loss Exponent
dispON = 0;                                 % Display Placements/Estimation
useKE = 1;                                  % Calculate Cost Function with
                                            % Known Emitter Power Values
useTime = 0;                                % Time the Script
assignS = 0;                                % Assign Sensors, 0 otherwise
assignE = 0;                                % Assign Emitter, 0 otherwise
maxShadowSpread = 12;                       % Maximum Shadow Spread


%%
% Main Simulation
% 4 different plots in one so 3rd dimention is 4
estErr = zeros(trialSize,maxShadowSpread,4);
meanEstErr = zeros(1,maxShadowSpread+1,4);

% 4x4 level 9
for SS=0:maxShadowSpread
    for i=1:trialSize            
        estErr(i,SS+1,1)...                
        = Main( ROI, gridSize, N_s, P_T, P_E, SS, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 9, 1);
    end
    disp(['Shadow Spread ', num2str(SS) , ' is complete.']);
end
% 4x4 level 4
for SS=0:maxShadowSpread
    for i=1:trialSize            
        estErr(i,SS+1,2)...                
        = Main( ROI, gridSize, N_s, P_T, P_E, SS, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 4, 1);
    end
    disp(['Shadow Spread ', num2str(SS) , ' is complete.']);
end
% 2x2 level 9
for SS=0:maxShadowSpread
    for i=1:trialSize            
        estErr(i,SS+1,3)...                
        = Main( ROI, gridSize, N_s, P_T, P_E, SS, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 9, 0);
    end
    disp(['Shadow Spread ', num2str(SS) , ' is complete.']);
end

% Full Grid Search
for SS=0:maxShadowSpread
    for i=1:trialSize            
        estErr(i,SS+1,4)...                
        = Main( ROI, gridSize, N_s, P_T, P_E, SS, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 0, 0);
    end
    disp(['Shadow Spread ', num2str(SS) , ' is complete.']);
end

%%
% Calculation of mean estimated error values for different shadow spread.
for SS = 0:maxShadowSpread
    if(dispON)
        disp(['Mean estimated error for shadow spread'...
            , num2str(SS), ' is ' ...
            , num2str(sum(estErr(:,SS+1))/trialSize)]);
    end
    for k=1:4
        meanEstErr(1,SS+1,k) = sum(estErr(:,SS+1,k))/trialSize;
    end
end

%%
% Visualization of the estimated error data for different area of
% interest and shadow spread values. We will plot mean estimation error
% versus shadow spread and plot this for different area of interest values 
% on top of each other.
if(useKE)
    figure('name','IGS Known Emitter','numbertitle','off');
else
    figure('name','IGS Unknown Emitter','numbertitle','off');
end
plot(0:maxShadowSpread, meanEstErr(1,:,1), 'x-'...
    ,0:maxShadowSpread, meanEstErr(1,:,2), '.-'...
    ,0:maxShadowSpread, meanEstErr(1,:,3), '*-'...
    ,0:maxShadowSpread, meanEstErr(1,:,4), 'h-');
grid on;
title({...
    'Mean Estimation Error for Different Shadow Spread of 2x2 IGS' ...
    , strcat('ROI = ', num2str(ROI), ', N_s = ', num2str(N_s) ...
    , ', trialSize = ', num2str(trialSize), ', P_T = ', num2str(P_T) ...
    , ', P_E = ', num2str(P_E), ', alpha = ', num2str(alpha) ...
    , ', useKE = ', num2str(useKE))});
xlabel('Shadow spread (dB)');
ylabel('Mean Estimation error (m)');
hLegend = legend('4x4 9'...
               ,'4x4 4'...
               ,'2x2 9'...
               ,'Full Grid Search');
v = get(hLegend, 'title');            
set(v,'string', 'Types of Runs');

end

%% Mean Estimation Error vs Number of Sensor
% Following function creates the figure 5 in the title making use of
% the "Main" function.

function [] = IGS_MEEvsNofS()
%%
% Below we initialize the figure parameters. X-axis is number of sensors 
% and Y-axis is the mean estimation error. Number of calculations before 
% taking the estimated value for mean error is trialSize and X-axis starts 
% from 20 and continues until 80.


%%
% Initilization of variables
ROI = 1000;                                 % Area of Interest
gridSize = 10;                              % Grid Element Size
trialSize = 1000;                           % Number of Function Calls
P_T = 1;                                    % Transmit Power of the Emitter
P_E = 1;                                    % Assumed Transmit Power
sigma = 6;                                  % Shadow Spread (Watts)
alpha = 3.5;                                % Path Loss Exponent
dispON = 0;                                 % Display Placements/Estimation
useKE = 1;                                  % Calculate Cost Function with
                                            % Known Emitter Power Values                                            
useTime = 0;                                % Time the Script
assignS = 0;                                % Assign Sensors, 0 otherwise
assignE = 0;                                % Assign Emitter, 0 otherwise
N_s = 20:10:80;                             % Number of Sensors


%%
% Main Simulation
% 4 different plots in one so 3rd dimention is 4
estErr = zeros(trialSize,length(N_s),4);
meanEstErr = zeros(1,length(N_s),4);

% 4x4 level 9
disp('Simulation for IGS 4x4 level 9');
for k=N_s
    for i=1:trialSize            
        estErr(i,find(N_s==k),1)...                
        = Main( ROI, gridSize, k, P_T, P_E, sigma, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 9, 1);
    end
    disp([ num2str(k) , ' sensors simulation is complete.']);
end
% 4x4 level 4
disp('Simulation for IGS 4x4 level 4');
for k=N_s
    for i=1:trialSize            
        estErr(i,find(N_s==k),2)...                
        = Main( ROI, gridSize, k, P_T, P_E, sigma, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 4, 1);
    end
    disp([ num2str(k) , ' sensors simulation is complete.']);
end
% 2x2 level 9
disp('Simulation for IGS 2x2 level 9');
for k=N_s
    for i=1:trialSize            
        estErr(i,find(N_s==k),3)...                
        = Main( ROI, gridSize, k, P_T, P_E, sigma, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 9, 0);
    end
    disp([ num2str(k) , ' sensors simulation is complete.']);
end

% Full Grid Search
disp('Simulation for FGS');
for k=N_s
    for i=1:trialSize            
        estErr(i,find(N_s==k),4)...                
        = Main( ROI, gridSize, k, P_T, P_E, sigma, alpha...
                , dispON...
                , useTime, useKE...
                , assignS, assignE...
                , 0, 0);
    end
    disp([ num2str(k) , ' sensors simulation is complete.']);
end

%%
% Calculation of mean estimated error values for different shadow spread.
for t = N_s
    if(dispON)
        disp(['Mean estimated error for shadow spread '...
            , num2str(t), ' is ' ...
            , num2str(sum(estErr(:,find(N_s==t)))/trialSize)]);
    end
    for k=1:4
        meanEstErr(1,find(N_s==t),k)...
            = sum(estErr(:,find(N_s==t),k))/trialSize;
    end
end

%%
% Visualization of the estimated error data for different area of
% interest and shadow spread values. We will plot mean estimation error
% versus shadow spread and plot this for different area of interest values 
% on top of each other.
if(useKE)
    figure('name','IGS Known Emitter','numbertitle','off');
else
    figure('name','IGS Unknown Emitter','numbertitle','off');
end
plot(N_s, meanEstErr(1,:,1), 'x-'...
    ,N_s, meanEstErr(1,:,2), '.-'...
    ,N_s, meanEstErr(1,:,3), '*-'...
    ,N_s, meanEstErr(1,:,4), 'h-');
grid on;
title({...
    'Mean Estimation Error for Different Number of sensors' ...
    , strcat('ROI = ', num2str(ROI), ', sigma = ', num2str(sigma) ...
    , ', trialSize = ', num2str(trialSize), ', P_T = ', num2str(P_T) ...
    , ', P_E = ', num2str(P_E), ', alpha = ', num2str(alpha) ...
    , ', useKE = ', num2str(useKE))});
xlabel('Number of Sensors');
ylabel('Mean Estimation error (m)');
hLegend = legend('4x4 9'...
               ,'4x4 4'...
               ,'2x2 9'...
               ,'Full Grid Search');
v = get(hLegend, 'title');            
set(v,'string', 'Types of Runs');

end

%% Main Function
% This function implements a single estimation of an emitter using sensors
% that are placed randomly. First a gridded coordinate system is created.
% Then sensor and emitter locations are assigned. After placement received
% signal power values are created. Using these values and the known sensor
% locations emitters location is estimated. The error of estimation is
% found and returned at the end of the function.
%
% If plotON is given as 0 nothing will be displayed and there will be no
% plots. Changing it to something larger than 1 will create a plot multiple
% subplots that shows the cost function with different vertical zoom levels
% . Also sensor and emitter placements and the estimation is displayed.

function [estErr, sPos, ePos]...
                    = Main( ROI, gridSize, N_s, P_T, P_E, sigma, alpha...
                    , dispON...
                    , useTime, useKE...
                    , assignS, assignE...
                    , maxLevel, is4by4)                  

% Placing Sensors/Emitter and Creating Sensor Distance Matrix
[sPos, ePos] = pse(assignS, assignE, dispON, N_s, ROI, gridSize);

if useTime
    disp(['Placing sensors and emitter took '...
                , num2str(toc(pseTime)), ' seconds']);
    crssTime = tic();
end

% Creating Received Signal Strength Values
[r] = crss(sPos, ePos, P_T, sigma, alpha, N_s);

if useTime
    disp(['Creating RSS took ' num2str(toc(crssTime)) ' seconds']);
    ctTime = tic();
end

% Emitter Location Estimation
[estErr, estELoc] = ele( ROI, N_s, r, useKE, P_E, alpha, sPos, ePos ...
                                , dispON, gridSize, maxLevel, is4by4);

if(dispON)
    plotSELocations(ROI, gridSize, sPos, ePos, estELoc);
end

if useTime
    disp(['Calculating theta took ' num2str(toc(ctTime)) ' seconds']);
    disp(['Total elapsed time is ' num2str(toc(ccsTime)) ' seconds']);
end

end

%% Placing Sensors/Emitter and Creating Sensor Distance Matrix
% Making use of the vectorized coordinate system we will put the sensors
% and the emitter in the simulation and create the distance matrices.
% Sensors and the emi   tter is placed randomly. They are selected so that
% none of them are on the same coordinates.
%
% The number of sensors is defined here. To change the number of sensors we
% can increase N_s. Sensor and emitter locations are assigned randomly.
% They are never in the same coordinates.
function [sPos, ePos] = pse(assignS, assignE, dispON, N_s, ROI, gridSize)

if assignS
    sPos = assignS;
    
    if (dispON>0)
        for i=1:N_s        
            disp(['Sensor '...              % Display selected coordinates
                    , num2str(i), ' is located at (', num2str(sPos(1,i))...
                    ,',', num2str(sPos(2,i)), ').']);
        end
    end
else    
    % Sensor positions on (x,y) plane
    sPos = zeros(2,N_s);
    for i=1:N_s
        isDistinct = 0;                     % Boolean flag
        while ~isDistinct
            sPos(:,i) = [rand()*ROI; rand()*ROI];
            %sPos(:,i) = [randi([0 ROI-1], 1, 1); randi([0 ROI-1], 1, 1)];
            isDistinct = 1;                 % True unless proven otherwise
            for j=1:i-1
                if (abs(sPos(1,i)-sPos(1,j))<gridSize ...
                    && abs(sPos(2,i)-sPos(2,j))<gridSize)
                    isDistinct = 0;         % Shown that it is not distinct
                end
            end
        end
        if (dispON>0)
            disp(['Sensor '...              % Display selected coordinates
                    , num2str(i), ' is located at (', num2str(sPos(1,i))...
                    ,',', num2str(sPos(2,i)), ').']);
        end
    end
end
if assignE
    ePos = assignE;
    if (dispON>0)
        disp(['Emitter is placed at ('...   % Display selected coordinates
                 , num2str(ePos(1,1)) ,',', num2str(ePos(2,1)), ').']);
    end
else    
    % Emitter position on (x,y) plane
    ePos = zeros(2,1);
    isDistinct = 0;                         % Boolean flag
    while ~isDistinct
        ePos(:,1) = [rand()*ROI; rand()*ROI];
        %ePos(:,1) = [randi([0 ROI-1], 1, 1); randi([0 ROI-1], 1, 1)];
        isDistinct = 1;                     % Unless proven otherwise, true
        for i=1:N_s
            if (abs(sPos(1,i)-ePos(1,1))<gridSize ...
                && abs(sPos(2,i)-ePos(2,1))<gridSize)
                % Shown that it is not distinct
                isDistinct = 0;
            end
        end
    end

    if (dispON>0)
        disp(['Emitter is placed at ('...   % Display selected coordinates
                 , num2str(ePos(1,1)) ,',', num2str(ePos(2,1)), ').']);
    end
end

end

%% Creating Received Signal Strength Values
% These variables are required to create the signal model 
% $m_i =  (P_T) (d_i)^{- \alpha}$ in Watts where 
% $1 \geq i \geq N_s$, $$P_T$ is the the transmit power of the emitter, 
% $\alpha$ is the path loss exponent and 
% $d_i=\sqrt{(x_i-x_0)^2 + (y_i-y_0)^2}$ is the distance between the
% transmitter and the ith sensor.
%
% The sensor's experience log-normal shadowing. If the fast fading effects
% are sufficiently averaged over time then the resulting unknown measured
% power from the emitter to the ith sensor is given as 
% $r_i = 10^{log_{10} (m_i) + \frac{\omega_i}{10}}$ where
% $\omega_i$ is a normal random variable with
% mean of 0 and a variance of $\sigma^2$.
function [r] = crss(sPos, ePos, P_T, sigma, alpha, N_s)

d = sqrt(...                                % Sensor and emitter Distance 
        (sPos(1,:)-ePos(1,:)).^2 + (sPos(2,:)-ePos(2,:)).^2);

m = P_T * d.^(-alpha);                      % Received signal in Watts
r = m;
for i=1:N_s
    r(i) = 10^(log10(m(i)) + normrnd(0,sigma)/10);
end

end

%% Emitter Location Estimation
% Probability of observing all sensors outputs given $\theta$ is written as
% $p(r|\theta)=\prod_{i=1}^{N_s} \frac{1}{\sqrt{2 \pi \sigma^2}}
% e^{-\frac{(r_i-m_i)^2}{2 \sigma^2}}$ where $\theta = (x_0,y_0)$ is the
% emitter location parameter to be estimated. ML estimate of the emitter
% location can be obtained by maximizing the (log)likelihood function which
% requires minimization of the sum of squared differences between each
% sensor's emitter power estimate and average of all sensor estimates:
% $\theta = min_{x,y} \sum_{i = 1}^{N_s} ( 10 log_{10}(r_i (d_i)^{\alpha}))
% -\frac{1}{N_s-1} \sum_{i=1}^{N_s} (10 log_{10} (r_i (d_i)^{\alpha})))^2$.
% For known emitter power values the used cost function is 
% $\sum_{i = 1}^{N_s} (10 log_{10}(r_i)-10log_{10}((P_{T_E}d^{-\alpha}))^2$

function [estErr, estELoc] = ele( ROI, N_s, r, useKE, P_E, alpha, sPos, ePos...
                                    , dispON, gridSize, maxLevel, is4by4)

sL = ROI;                                   % Side Length
% Left top is (0,0) for x increasing right and y increasing downwards
x = 0;
y = 0;

level = 1;

% Use Iterative until level or gridSize is reached
while(sL>gridSize && level<=maxLevel)    
    if(is4by4)
        % Coordinates to calculate theta on
        coords = zeros(2,4);
        coords(:,1) = [x+sL/8; y+sL/8];
        coords(:,2) = [x+sL/8; y+3*sL/8];
        coords(:,3) = [x+sL/8; y+5*sL/8];
        coords(:,4) = [x+sL/8; y+7*sL/8];
        coords(:,5) = [x+3*sL/8; y+sL/8];
        coords(:,6) = [x+3*sL/8; y+3*sL/8];
        coords(:,7) = [x+3*sL/8; y+5*sL/8];
        coords(:,8) = [x+3*sL/8; y+7*sL/8];
        coords(:,9) = [x+5*sL/8; y+sL/8];
        coords(:,10) = [x+5*sL/8; y+3*sL/8];
        coords(:,11) = [x+5*sL/8; y+5*sL/8];
        coords(:,12) = [x+5*sL/8; y+7*sL/8];
        coords(:,13) = [x+7*sL/8; y+sL/8];
        coords(:,14) = [x+7*sL/8; y+3*sL/8];
        coords(:,15) = [x+7*sL/8; y+5*sL/8];
        coords(:,16) = [x+7*sL/8; y+7*sL/8];

        % Distance from these points
        sDist = zeros(16,N_s);    
        for k=1:16
            for i=1:N_s
                sDist(k,i) = sqrt((sPos(1,i)-coords(1,k)).^2 ...
                                + (sPos(2,i)-coords(2,k)).^2);                
            end
        end

        % Calculate Theta on the coordinates above
        theta = zeros(1,16);
        if (useKE)
            for k=1:16
                for i=1:N_s
                    theta(1,k) = theta(1,k) + (10*log10(r(i)) ...
                                - 10*log10(P_E*sDist(k,i).^(-alpha))).^2;                    
                end
            end
        else    
            innerSum = zeros(1,16);
            for k=1:16                
                for i=1:N_s
                    innerSum(1,k) = innerSum(1,k) ...
                                    + 10*log10(r(i)*sDist(k,i).^alpha);                    
                end
                for i=1:N_s
                    theta(1,k) = theta(1,k) ...   
                                + (10*log10(r(i)*sDist(k,i).^alpha)...
                                -(1/(N_s))*innerSum(1,k)).^2;                 
                end
            end
        end

        % Find minimum theta and set the top left corner and new side length
        x = coords(1,(find(theta == min(theta))))-sL/8;
        y = coords(2,(find(theta == min(theta))))-sL/8;       
        sL = sL/4;              

        % Level complete, increase level
        if(dispON)
            disp(['Side Length is ', num2str(sL)...
                ', x = ' num2str(x) ', y = ' num2str(y)]);
            disp(['Level ' num2str(level) ' is complete']);
        end
        level = level + 1;
    else    
        % Coordinates to calculate theta on
        coords = zeros(2,4);
        coords(:,1) = [x+sL/4; y+sL/4];
        coords(:,2) = [x+sL/4; y+3*sL/4];
        coords(:,3) = [x+3*sL/4; y+sL/4];
        coords(:,4) = [x+3*sL/4; y+3*sL/4];   

        % Distance from these points
        sDist = zeros(4,N_s);    
        for i=1:N_s
            sDist(1,i) = sqrt((sPos(1,i)-coords(1,1)).^2 ...
                            + (sPos(2,i)-coords(2,1)).^2);
            sDist(2,i) = sqrt((sPos(1,i)-coords(1,2)).^2 ...
                            + (sPos(2,i)-coords(2,2)).^2);
            sDist(3,i) = sqrt((sPos(1,i)-coords(1,3)).^2 ...
                            + (sPos(2,i)-coords(2,3)).^2);
            sDist(4,i) = sqrt((sPos(1,i)-coords(1,4)).^2 ...
                            + (sPos(2,i)-coords(2,4)).^2);
        end

        % Calculate Theta on the coordinates above
        theta = zeros(1,4);    
        if (useKE)
            for i=1:N_s
                theta(1,1) = theta(1,1) + (10*log10(r(i)) ...
                                - 10*log10(P_E*sDist(1,i).^(-alpha))).^2;
                theta(1,2) = theta(1,2) + (10*log10(r(i)) ...
                                - 10*log10(P_E*sDist(2,i).^(-alpha))).^2;
                theta(1,3) = theta(1,3) + (10*log10(r(i)) ...
                                - 10*log10(P_E*sDist(3,i).^(-alpha))).^2;
                theta(1,4) = theta(1,4) + (10*log10(r(i)) ...
                                - 10*log10(P_E*sDist(4,i).^(-alpha))).^2;
            end
        else    
            innerSum = zeros(1,4);
            for i=1:N_s
                innerSum(1,1) = innerSum(1,1) ...
                                + 10*log10(r(i)*sDist(1,i).^alpha);
                innerSum(1,2) = innerSum(1,2) ...
                                + 10*log10(r(i)*sDist(2,i).^alpha);
                innerSum(1,3) = innerSum(1,3) ...
                                + 10*log10(r(i)*sDist(3,i).^alpha);
                innerSum(1,4) = innerSum(1,4) ...
                                + 10*log10(r(i)*sDist(4,i).^alpha);
            end
            for i=1:N_s
                theta(1,1) = theta(1,1) ...                        
            + (10*log10(r(i)*sDist(1,i).^alpha)-(1/(N_s))*innerSum(1,1)).^2;
                theta(1,2) = theta(1,2) ...                
            + (10*log10(r(i)*sDist(2,i).^alpha)-(1/(N_s))*innerSum(1,2)).^2;
                theta(1,3) = theta(1,3) ...                
            + (10*log10(r(i)*sDist(3,i).^alpha)-(1/(N_s))*innerSum(1,3)).^2;
                theta(1,4) = theta(1,4) ...                       
            + (10*log10(r(i)*sDist(4,i).^alpha)-(1/(N_s))*innerSum(1,4)).^2;
            end
        end

        % Find minimum theta and set the top left corner and new side length
        x = coords(1,(find(theta == min(theta))))-sL/4;
        y = coords(2,(find(theta == min(theta))))-sL/4;       
        sL = sL/2;              

        % Level complete, increase level
        if(dispON)
            disp(['Side Length is ', num2str(sL)...
                ', x = ' num2str(x) ', y = ' num2str(y)]);
            disp(['Level ' num2str(level) ' is complete']);
        end
        level = level + 1;
    end
end
% Fix side length back if smaller than grid size
if(sL<gridSize)
    sL = sL*2;
end
%% 
% Start full grid search in the new system. Create a new system taking new 
% x,y as left top point
u = x:1:x+sL;                       % Locations on x axis
v = y:1:y+sL;                       % Locations on y axis
[gx,gy] = meshgrid(u, v);             % 2D plane indices for gx and gy

% Divide into grids
newSL = ceil(sL/gridSize);                % Side Length with grid
xGrid = zeros(newSL ,newSL );
yGrid = zeros(newSL ,newSL );

for k=0:gridSize:sL-1
    xGrid(:,(k/gridSize+1)) ...
        = gx(1:newSL , k+1)+gridSize/2;
    yGrid((k/gridSize+1),:) ...
        = gy(k+1,1:newSL)+gridSize/2;
end

% Distance from Sensor matrices
sDist = zeros(newSL, newSL, N_s);
for i=1:N_s
    sDist(:,:,i) = sqrt((sPos(1,i)-xGrid).^2 + (sPos(2,i)-yGrid).^2);
end

%%
% Calculate the objective function for each point in the new system

innerSum =...                               % Mean of estimated power
        zeros(newSL,newSL); 
theta = zeros(newSL,newSL);       

if (useKE)        
    for i=1:N_s
        theta = theta + (10*log10(r(i))...
                - 10*log10(P_E*sDist(:,:,i).^(-alpha))).^2;
    end
else    
    for i=1:N_s
        innerSum = innerSum + 10*log10(r(i)*sDist(:,:,i).^alpha);
    end
    for i=1:N_s
        theta = theta ...                
        + (10*log10(r(i)*sDist(:,:,i).^alpha)-(1/(N_s))*innerSum).^2;
        %+ (10*log10(r(i)*sDist(:,:,i).^alpha)-(1/(N_s-1))*innerSum).^2;               
    end
end

%%
% Finding of the indices of the minimum value of the $\theta$ function 
% is given below.

[Iy,Ix]=find(theta==min(min(theta)));

ex = xGrid(1,Ix);
ey = yGrid(Iy,1);
if length(ex)>1
    disp([num2str(length(ex)), ' possible solutions are found']);
end
if dispON>0
    for i=1:length(ex)
        disp(['Emitter found at (', num2str(ex(i))...
                , ',', num2str(ey(i)), ').']);
    end
end

% Take the first of the possible solutions as the solution
ex = ex(1);
ey = ey(1);

% Assign estimated emitter location
estELoc = [ex;ey];

%%
% Now we have both the emitter location and the estimation, we can find
% the error.
             
estErr = sqrt((ePos(1,:)-ex).^2 + (ePos(2,:)-ey).^2);
if dispON>0
    disp(['Estimation error is ', num2str(estErr)  , ' meters.']);
end

end

function [] = plotSELocations(ROI, gridSize, sPos, ePos, estELoc)
%% PLOTSELOCATIONS plots sensors and emittors on figure to show locations
% Required inputs are;
% - Region of interest axis length (ROI)
% - Grid axis length (gridSize)
% - Sensor positions given as [x;y] (sPos)
% - Emitter positions given as [x;y] (ePos)

% Create whole area
axis([0 ROI+1 0 ROI+1]);
hold on;

% Place actual sensors
scatter(sPos(1,:),sPos(2,:), 70, 'x');

% Place actual emitters also determine color for each
c(1,:)= [round(rand), round(rand), round(rand)];
while isequal(c(1,:),[1 1 1])
    c(1,:)= [round(rand), round(rand), round(rand)];
end

scatter(ePos(1,1),ePos(2,1), 70, 's' ...
            , 'MarkerFaceColor', c(1,:) ...
            , 'MarkerEdgeColor', c(1,:));

% Place estimated emitter locations
scatter(estELoc(1,1), estELoc(2,1), 70, 'd'...
            , 'MarkerFaceColor', c(1,:)...
            , 'MarkerEdgeColor', c(1,:));

hold off;
% Set grid on with the given size
tickValues = 0:gridSize:ROI+1;
set(gca,'YTick',tickValues)
set(gca,'XTick',tickValues)
grid on;

% Set title, labels and legend
title('Sensor and Emitter placements for IGS_{SE}');
xlabel('x');
ylabel('y');
legend('Sensors', 'Emitter', 'Estimated Emitter');
end