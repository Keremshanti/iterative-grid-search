%% Iterative Grid Search for RSS-Based Emitter Localization
% Main function implements the Full Grid search method provided in the 
% article given in the title. The variables required as input to the
% function is described below. Detailed descriptions of these variables are
% given in under the "Main Function" section.
%
% <html>
% <table border=1>
% <tr><td>Name</td>       <td>Description</td></tr>
% <tr><td>ROI</td>        <td>Region of Interest</td></tr>
% <tr><td>gridSize</td>   <td>Grid Size</td></tr>
% <tr><td>N_s</td>        <td>Number of Sensors</td></tr>
% <tr><td>P_T</td>        <td>Real Emitter Transmit Power</td></tr>
% <tr><td>sigma</td>      <td>Shadow Spread</td></tr>
% <tr><td>alpha</td>      <td>Path Loss Exponent</td></tr>
% <tr><td>plotON</td>     <td>Plot Zoom Level</td></tr>
% <tr><td>dispON</td>     <td>Display placement and estimation</td></tr>
% <tr><td>assignS</td>    <td>Assign sensors, 0 otherwise</td></tr>
% <tr><td>assignE</td>    <td>Assign emitter, 0 otherwise</td></tr>
% </table>
% </html>
%
% Dependencies for Plots:
% makedatatip.m
% subtightplot.m

%% Start Function
% This is the first function that is run when executed. Uncomment the
% relevant function to work with.
function [] = FGS_SE()
close all;
clear all;
clc;

Test();
%TestEmitter();
%FGS_MEEvsSS_VaryingGS();
%findLargeErr(20);

end


function [] = Test()
%% Example Test Function
% This function initilizes the variables for the main function.

    % Main simulation parameters
    ROI = 100;                               % Area of Interest    
    gridSize = 10;                           % Grid Element Size
    N_s = 4;                                % Number of Sensors
    P_T = 1;                                % Transmit Power of the Emitter
    P_E = 1;                                % Assumed Transmit Power
    sigma = 0;                              % Shadow Spread (Watts)
    alpha = 3.5;                            % Path Loss Exponent
    
    % Output Flags
    plotON = 1;                             % Number of Zoomed Plots
    dispON = 1;                             % Display Placements/Estimation
    tableON = 1;                            % Display the Cost Table    
    useKE = 1;                              % Calculate Cost Function with
                                            % Known Emitter Power Values
    useTime = 0;                            % Time the Script
    
    % Display simulation paramaters if flag set
    if(dispON)
        disp('Full Grid Simulation Single Emitter');
        disp(['Region of Interest is: ' ...
                , num2str(ROI), 'x',  num2str(ROI)]);
        disp(['Region is divided into grids of size: '...
                , num2str(gridSize), 'x',  num2str(gridSize)]); 
        disp(['Number of sensors: ', num2str(N_s)]);        
        disp(['Path Loss Exponent: ', num2str(alpha)]);
        disp(['Shadow Spread (dB): ', num2str(sigma)]);
    end
    
    % TEST Purposes
    sPos=0;
%     sPos(:,1) = [7.195;8.1725];
%     sPos(:,2) = [3.025;8.9505];    
%     sPos(:,3) = [8.3789;4.5527];
%     sPos(:,4) = [0.82942;7.4409];    
%     for k=5:N_s
%         sPos(:,k) = [rand()*ROI;rand()*ROI];
%     end    
    assignS = sPos;                  % Assign Sensors, 0 otherwise
    % Assign Emitter Locations
    ePos=0;    
    %ePos(:,1) = [7.3978;5.9];
    assignE = ePos;                  % Assign Sensors, 0 otherwise

%% Main Function Call
    Main( ROI, gridSize, N_s, P_T, P_E, sigma, alpha...
        , plotON, dispON, tableON...
        , useTime, useKE...
        , assignS, assignE);    
end

function [] = TestEmitter()
%% Puts Emitter over all the region of interest to display errored ones

    % Main simulation parameters
    ROI = 11;                               % Area of Interest    
    gridSize = 1;                           % Grid Element Size
    N_s = 3;                                % Number of Sensors
    P_T = 1;                                % Transmit Power of the Emitter
    P_E = 1;                                % Assumed Transmit Power
    sigma = 0;                              % Shadow Spread (Watts)
    alpha = 3.5;                            % Path Loss Exponent
    
    % Output Flags
    plotON = 0;                             % Number of Zoomed Plots
    dispON = 0;                             % Display Placements/Estimation
    tableON = 0;                            % Display the Cost Table    
    useKE = 0;                              % Calculate Cost Function with
                                            % Known Emitter Power Values
    useTime = 0;                            % Time the Script
    
    % Display simulation paramaters if flag set    
    disp('Full Grid Simulation Single Emitter');
    disp(['Region of Interest is: ' ...
            , num2str(ROI), 'x',  num2str(ROI)]);
    disp(['Region is divided into grids of size: '...
            , num2str(gridSize), 'x',  num2str(gridSize)]); 
    disp(['Number of sensors: ', num2str(N_s)]);        
    disp(['Path Loss Exponent: ', num2str(alpha)]);
    disp(['Shadow Spread (dB): ', num2str(sigma)]);    
    
    %Assign Sensor Locations
    sPos(:,1) = [3.025;8.9505];
    sPos(:,2) = [8.3789;4.5527];
    sPos(:,3) = [0.82942;7.4409];
%     sPos(:,1) = [7.195;8.1725];
%     sPos(:,2) = [3.025;8.9505];
%     sPos(:,3) = [8.3789;4.5527];
%     sPos(:,4) = [0.82942;7.4409];
    for k=5:N_s
        sPos(:,k) = [rand()*ROI;rand()*ROI];
    end    
    % Assign Sensors, 0 otherwise
    assignS = sPos;                         

    % Assign Emitter location to everywhere and see if we estimate its
    % location in the same grid
    resolution = 0.05;
    h = waitbar(0,'Populating figure');     
    errorMapIndex = 1;
    for k=0:resolution:ROI-1
        for l=0:resolution:ROI-1
            ePos(:,1) = [k;l];
            assignE = ePos;                  
            estError  =  Main( ROI, gridSize, N_s, P_T, P_E, sigma, alpha...
                                , plotON, dispON, tableON...
                                , useTime, useKE...
                                , assignS, assignE);    
            if(estError>sqrt(gridSize)/2)
                errorMapX(errorMapIndex) = k;
                errorMapY(errorMapIndex) = l;
                errorMapIndex = errorMapIndex + 1;
            end
        end        
        waitbar((k+resolution)/(ROI-1+resolution) ...
                    ,h,'Populating Figure:');
    end
    close(h);
    
    % Assign values to base to be able to redraw using same values
    assignin('base','errorMapX', errorMapX);
    assignin('base','errorMapY', errorMapY);
    assignin('base','sPos', sPos);
    assignin('base','gridSize', gridSize);
    assignin('base','ROI', ROI);
    %% Start Ploting on a scattermap
    
    % Create whole area
    axis([0 ROI-1 0 ROI-1]);
    hold on;
    % Place errored emitters
    scatter(errorMapX,errorMapY, 3, 's'...
            , 'MarkerFaceColor', [0 1 1]...
            , 'MarkerEdgeColor', [0 1 1]);
    % Place actual sensors
    scatter(sPos(1,:),sPos(2,:), 70, 'c'...
            , 'MarkerFaceColor', [1 0 0]...
            , 'MarkerEdgeColor', [1 0 0]);
    hold off;
    
    % Set grid on with the given size
    tickValues = 0:gridSize:ROI+1;
    set(gca,'YTick',tickValues)
    set(gca,'XTick',tickValues)
    %grid on;
    
    % Set title, labels and legend
    title('Errored Sensor Locations for FGS_{SE}');
    xlabel('x');
    ylabel('y');    
end

function [] = FGS_MEEvsSS_VaryingGS()
%% Simulation For Changing Grid Size
%
% Here we call the function for 6 different area of interest sizes and 12
% different shadow spread values trialSize times and find the estimated 
% error for each. This creates a large 3D estimated error matrix.
% Area of interest is AOIxAOI
% Grid size varies
% Number of sensors is N_s
% Shadow spread varies
% Path loss exponent is alpha

% Below we initialize the figure parameters. X-axis is shadow spread and
% Y-axis is the mean estimation error. Number of calculations before taking
% the estimated value for mean error is trialSize and Y-axis starts from 0
% and continues until maxShadowSpread. The area of interest size is denoted
% as GS_s.

ROI = 1000;                                 % Area of Interest
N_s = 50;                                   % Number of Sensors
trialSize = 1000;                           % Number of Function Calls
P_T = 1;                                    % Transmit Power of the Emitter
P_E = 1;                                    % Assumed Transmit Power
alpha = 3.5;                                % Path Loss Exponent
plotON = 0;                                 % Number of Zoomed Plots
dispON = 0;                                 % Display Placements/Estimation
tableON = 0;                                % Display the Cost Table
useKE = 0;                                  % Calculate Cost Function with
                                            % Known Emitter Power Values
useTime = 0;                                % Time the Script
assignS = 0;                                % Assign Sensors, 0 otherwise
assignE = 0;                                % Assign Emitter, 0 otherwise

maxShadowSpread = 12;                       % Maximum Shadow Spread
GS_s = zeros(6,1);                          % Grid Sizes
GS_s(1) = 10;
GS_s(2) = 20;
GS_s(3) = 25;
GS_s(4) = 40;
GS_s(5) = 50;
GS_s(6) = 100;



estErr = zeros(trialSize,maxShadowSpread,6);
meanEstErr = zeros(1,maxShadowSpread+1,6);
for GS=1:6
    for SS=0:maxShadowSpread
        for i=1:trialSize            
            estErr(i,SS+1,GS)...                
            = Main( ROI, GS_s(GS), N_s, P_T, P_E, SS, alpha...
                    , plotON, dispON, tableON...
                    , useTime, useKE...
                    , assignS, assignE);            
        end
        disp(['Shadow Spread ', num2str(SS) , ' is complete.']);
    end
    disp(['Grid Size ', num2str(GS_s(GS)), ...
        'x', num2str(GS_s(GS)) , ' is complete.']);
end

% Calculation of mean estimated error values for different shadow spread.
for GS=1:6
    for SS = 0:maxShadowSpread
        disp(['Mean estimated error for shadow spread '...
                , num2str(SS), ' is ' ...
                , num2str(sum(estErr(:,SS+1,GS))/trialSize)]);        
        meanEstErr(1,SS+1,GS) = sum(estErr(:,SS+1,GS))/trialSize;
    end
end

% Now we need to visualize the estimated error data for different area of
% interest and shadow spread values. We will plot mean estimation error
% versus shadow spread and plot this for different area of interest values 
% on top of each other.

if(useKE)
    figure('name','FGS Known Emitter','numbertitle','off');
else
    figure('name','FGS Unknown Emitter','numbertitle','off');
end

plot(0:maxShadowSpread, meanEstErr(1,:,1), 'x-'...
    ,0:maxShadowSpread, meanEstErr(1,:,2), '.-'...
    ,0:maxShadowSpread, meanEstErr(1,:,3), '*-'...
    ,0:maxShadowSpread, meanEstErr(1,:,4), 'h-'...
    ,0:maxShadowSpread, meanEstErr(1,:,5), 'v-'...
    ,0:maxShadowSpread, meanEstErr(1,:,6), 'p-');
grid on;
title({...
    'Mean Estimation Error for Different Shadow Spread and Grid Sizes' ...
    , strcat('ROI = ', num2str(ROI), ', N_s = ', num2str(N_s) ...
    , ', trialSize = ', num2str(trialSize), ', P_T = ', num2str(P_T) ...
    , ', P_E = ', num2str(P_E), ', alpha = ', num2str(alpha) ...
    , ', useKE = ', num2str(useKE))});
xlabel('Shadow spread (dB)');
ylabel('Mean Estimation error (m)');
hLegend = legend('10x10'...
                ,'20x20'...
                ,'25x25'...
                ,'40x40'...
                ,'50x50'...
                ,'100x100');
v = get(hLegend, 'title');            
set(v,'string', 'Grid Sizes');

end

function [sPos, ePos] = findLargeErr(threshold)   
%% Find large error
% Finds a sensor/emitter location assignment that results in an error 
% larger than given error as parameter and displays the sensor/emitter
% placement in a copy/pastable manner.

    ROI = 1000;                             % Area of Interest
    gridSize = 2;                           % Grid Element Size
    N_s = 4;                                % Number of Sensors
    P_T = 1;                                % Transmit Power of the Emitter
    P_E = 1;                                % Assumed Transmit Power
    sigma = 0;                              % Shadow Spread (Watts)
    alpha = 3.5;                            % Path Loss Exponent
    plotON = 0;                             % Number of Zoomed Plots
    dispON = 0;                             % Display Placements/Estimation
    tableON = 0;                            % Display the Cost Table    
    useKE = 0;                              % Calculate Cost Function with
                                            % Known Emitter Power Values
    useTime = 0;                            % Time the Script    
    assignS = 0;                            % Assign Sensors, 0 otherwise
    assignE = 0;                            % Assign Emitter, 0 otherwise
    
    estErr = 0;
    counter = 0;
    while(estErr<threshold)
        [estErr, sPos, ePos] ...
            = Main( ROI, gridSize, N_s, P_T, P_E, sigma, alpha...
            , plotON, dispON, tableON...
            , useTime, useKE...
            , assignS, assignE);        
        counter=counter+1;      
        if(~mod(counter,10))
            disp(['Number of tries is ' num2str(counter)]);
        end
    end
    %disp(['Estimated error is ' num2str(estErr)...
    %     ' for the sensor/emitter placement;'])   
    disp(['sPos(:,1) = [' num2str(sPos(1,1)) ';' num2str(sPos(2,1)) '];']);
    disp(['sPos(:,2) = [' num2str(sPos(1,2)) ';' num2str(sPos(2,2)) '];']);
    disp(['sPos(:,3) = [' num2str(sPos(1,3)) ';' num2str(sPos(2,3)) '];']);
    disp(['sPos(:,4) = [' num2str(sPos(1,4)) ';' num2str(sPos(2,4)) '];']);
    %disp('assignS = sPos;                  % Assign Sensors, 0 otherwise')
    %disp('% Assign Emitter Locations')
    disp(['ePos(:,1) = [' num2str(ePos(1,1)) ';' num2str(ePos(2,1)) '];']);
    %disp('assignE = ePos;                  % Assign Sensors, 0 otherwise')
    
    axis([0 ROI 0 ROI]);
    hold on;
    scatter(sPos(1,:),sPos(2,:), 70, 'x');
    scatter(ePos(1,:),ePos(2,:), 70);
    hold off;
    grid on;
    title({...
    strcat('Sensor/Emitter Placement with Error ', estErr, ' meters.')...
    , strcat('ROI = ', num2str(ROI), ', N_s = ', num2str(N_s) ...
    , ', P_T = ', num2str(P_T), ', P_E = ', num2str(P_E), ', alpha = ' ...
    , num2str(alpha), ', useKE = ', num2str(useKE))});
    xlabel('x');
    ylabel('y');
    
end


function [estErr, sPos, ePos]...
                    = Main( ROI, gridSize, N_s, P_T, P_E, sigma, alpha...
                    , plotON, dispON, tableON...
                    , useTime, useKE...
                    , assignS, assignE)                  
%% Main Function
% This function implements a single estimation of an emitter using sensors
% that are placed randomly. First a gridded coordinate system is created.
% Then sensor and emitter locations are assigned. After placement received
% signal power values are created. Using these values and the known sensor
% locations emitters location is estimated. The error of estimation is
% found and returned at the end of the function.
%
% If plotON is given as 0 nothing will be displayed and there will be no
% plots. Changing it to something larger than 1 will create a plot multiple
% subplots that shows the cost function with different vertical zoom levels
% . Also sensor and emitter placements and the estimation is displayed.

                
if useTime
    ccsTime = tic();
end

% Creating the coordinate system
[newROI, xGrid, yGrid] = ccs(ROI, gridSize);

if useTime
    disp(['Creating the coordinate system took '...
                ,num2str(toc(ccsTime)), ' seconds']);
    pseTime = tic();
end

% Placing Sensors/Emitter and Creating Sensor Distance Matrix
[sPos, ePos, sDist] = pse(assignS, assignE, dispON, N_s...
                            , xGrid, yGrid , ROI ,newROI, gridSize);

if useTime
    disp(['Placing sensors and emitter took '...
                , num2str(toc(pseTime)), ' seconds']);
    crssTime = tic();
end

% Creating Received Signal Strength Values
[r] = crss(sPos, ePos, P_T, sigma, alpha, N_s);

if useTime
    disp(['Creating RSS took ' num2str(toc(crssTime)) ' seconds']);
    ctTime = tic();
end

% Emitter Location Estimation
[estErr estELoc] = ele(newROI, N_s, r, sDist, useKE...
                                , P_E, alpha, tableON, ePos, xGrid, yGrid...
                                , plotON, dispON);

if(dispON)
    plotSELocations(ROI, gridSize, sPos, ePos, estELoc);
end
                            
if useTime
    disp(['Calculating theta took ' num2str(toc(ctTime)) ' seconds']);
    disp(['Total elapsed time is ' num2str(toc(ccsTime)) ' seconds']);
end

end

function [newROI, xGrid, yGrid] = ccs(ROI, gridSize)
%% Creating the coordinate system
% Here we create the 2D coordinate system as two matrices each including
% their associated indexes. This plane will be our Region of Interest(ROI).
% ROI is divided into grids to minimize computation.
%
% Vectorization in MATLAB is important as reallocation of memory takes a
% lot of time in simulations therefore everything should be predefined for
% best performance.

% Locations on x axis
u = 0:1:ROI-1;
% Locations on y axis
v = 0:1:ROI-1;
% 2D plane indices for x and y
[x,y] = meshgrid(u, v);

newROI = ceil(ROI/gridSize);                % ROI size with grid
xGrid = zeros(newROI ,newROI );
yGrid = zeros(newROI ,newROI );

for k=0:gridSize:ROI-1
    xGrid(:,(k/gridSize+1)) ...
        = x(1:newROI , k+1)+gridSize/2;
    yGrid((k/gridSize+1),:) ...
        = y(k+1,1:newROI)+gridSize/2;
end

end


function [sPos, ePos, sDist] = pse(assignS, assignE, dispON, N_s...
                                    , xGrid, yGrid , ROI ,newROI, gridSize)
%% Placing Sensors/Emitter and Creating Sensor Distance Matrix
% Making use of the vectorized coordinate system we will put the sensors
% and the emitter in the simulation and create the distance matrices.
% Sensors and the emitter is placed randomly. They are selected so that
% none of them are on the same coordinates.
%
% The number of sensors is defined here. To change the number of sensors we
% can increase N_s. Sensor and emitter locations are assigned randomly.
% They are never in the same coordinates.

if assignS
    sPos = assignS;
    
    if (dispON>0)
        for i=1:N_s        
            disp(['Sensor '...              % Display selected coordinates
                    , num2str(i), ' is located at (', num2str(sPos(1,i))...
                    ,',', num2str(sPos(2,i)), ').']);
        end
    end
else    
    % Sensor positions on (x,y) plane
    sPos = zeros(2,N_s);
    for i=1:N_s
        isDistinct = 0;                     % Boolean flag
        while ~isDistinct
            sPos(:,i) = [rand()*ROI;rand()*ROI];
            %sPos(:,i) = [randi([0 ROI-1], 1, 1); randi([0 ROI-1], 1, 1)];
            isDistinct = 1;                 % True unless proven otherwise
            for j=1:i-1
                if (abs(sPos(1,i)-sPos(1,j))<gridSize ...
                    && abs(sPos(2,i)-sPos(2,j))<gridSize)
                    isDistinct = 0;         % Shown that it is not distinct
                end                
            end
        end
        if (dispON>0)
            disp(['Sensor '...              % Display selected coordinates
                    , num2str(i), ' is located at (', num2str(sPos(1,i))...
                    ,',', num2str(sPos(2,i)), ').']);
        end
    end
end
if assignE
    ePos = assignE;
    if (dispON>0)
        disp(['Emitter is placed at ('...   % Display selected coordinates
                 , num2str(ePos(1,1)) ,',', num2str(ePos(2,1)), ').']);
    end
else    
    % Emitter position on (x,y) plane
    ePos = zeros(2,1);
    isDistinct = 0;                         % Boolean flag
    while ~isDistinct
        ePos(:,1) = [rand()*ROI;rand()*ROI];
        %ePos(:,1) = [randi([0 ROI-1], 1, 1); randi([0 ROI-1], 1, 1)];
        isDistinct = 1;                     % Unless proven otherwise, true
        for i=1:N_s
            if (abs(sPos(1,i)-ePos(1,1))<gridSize ...
                && abs(sPos(2,i)-ePos(2,1))<gridSize)
                % Shown that it is not distinct
                isDistinct = 0;
            end
        end
    end

    if (dispON>0)
        disp(['Emitter is placed at ('...   % Display selected coordinates
                 , num2str(ePos(1,1)) ,',', num2str(ePos(2,1)), ').']);
    end
end

% Distance from Sensor matrices
sDist = zeros(newROI, newROI, N_s);
for i=1:N_s
    sDist(:,:,i) = sqrt((sPos(1,i)-xGrid).^2 + (sPos(2,i)-yGrid).^2);
end

end


function [r] = crss(sPos, ePos, P_T, sigma, alpha, N_s)
%% Creating Received Signal Strength Values
% These variables are required to create the signal model 
% $m_i =  (P_T) (d_i)^{- \alpha}$ in Watts where 
% $1 \geq i \geq N_s$, $$P_T$ is the the transmit power of the emitter, 
% $\alpha$ is the path loss exponent and 
% $d_i=\sqrt{(x_i-x_0)^2 + (y_i-y_0)^2}$ is the distance between the
% transmitter and the ith sensor.
%
% The sensor's experience log-normal shadowing. If the fast fading effects
% are sufficiently averaged over time then the resulting unknown measured
% power from the emitter to the ith sensor is given as 
% $r_i = 10^{log_{10} (m_i) + \frac{\omega_i}{10}}$ where
% $\omega_i$ is a normal random variable with
% mean of 0 and a variance of $\sigma^2$.

% Sensor and emitter Distance 
d = sqrt((sPos(1,:)-ePos(1,:)).^2 + (sPos(2,:)-ePos(2,:)).^2);

m = P_T * d.^(-alpha);                  % Received signal in Watts
r = m;
for i=1:N_s
    r(i) = 10^(log10(m(i)) + normrnd(0,sigma)/10);
end

end

function [estErr, estELoc] = ele(newROI, N_s, r, sDist, useKE...
                                , P_E, alpha, tableON, ePos, xGrid, yGrid...
                                , plotON, dispON)
%% Emitter Location Estimation
% Probability of observing all sensors outputs given $\theta$ is written as
% $p(r|\theta)=\prod_{i=1}^{N_s} \frac{1}{\sqrt{2 \pi \sigma^2}}
% e^{-\frac{(r_i-m_i)^2}{2 \sigma^2}}$ where $\theta = (x_0,y_0)$ is the
% emitter location parameter to be estimated. ML estimate of the emitter
% location can be obtained by maximizing the (log)likelihood function which
% requires minimization of the sum of squared differences between each
% sensor's emitter power estimate and average of all sensor estimates:
% $\theta = min_{x,y} \sum_{i = 1}^{N_s} ( 10 log_{10}(r_i (d_i)^{\alpha}))
% -\frac{1}{N_s} \sum_{i=1}^{N_s} (10 log_{10} (r_i (d_i)^{\alpha})))^2$.
% For known emitter power values the used cost function is 
% $\sum_{i = 1}^{N_s} (10 log_{10}(r_i)-10log_{10}((P_{T_E}d^{-\alpha}))^2$


innerSum =...                               % Mean of estimated power
        zeros(newROI,newROI); 
theta = zeros(newROI,newROI);       

if (useKE)        
    for i=1:N_s
        theta = theta + (10*log10(r(i)) - 10*log10(P_E*sDist(:,:,i).^(-alpha))).^2;
    end
else    
    for i=1:N_s        
        innerSum = innerSum + 10*log10(r(i)*sDist(:,:,i).^alpha);
    end
    for i=1:N_s
        theta = theta ...                                        
        + (10*log10(r(i)*sDist(:,:,i).^alpha)-(1/N_s)*innerSum).^2;            
        %+ (10*log10(r(i)*sDist(:,:,i).^alpha)).^2;            
        %+ (10*log10(r(i)*sDist(:,:,i).^alpha)-(1/(N_s-1))*innerSum).^2;
    end
end

% Finding of the indices of the minimum value of the $\theta$ function 
% is given below.

[Iy,Ix]=find(theta==min(min(theta)));

ex = xGrid(1,Ix);
ey = yGrid(Iy,1);
if length(ex)>1
    disp([num2str(length(ex)), ' possible solutions are found']);
end
if dispON>0
    for i=1:length(ex)
        disp(['Emitter found at (', num2str(ex(i))...
                , ',', num2str(ey(i)), ').']);
    end
end
ex = ex(1);
ey = ey(1);

% Assign estimated emitter location
estELoc = [ex;ey];

% Now we have both the emitter location and the estimation, we can find
% the error.
estErr = sqrt((ePos(1,:)-ex).^2 + (ePos(2,:)-ey).^2);
if dispON>0
    disp(['Estimation error is ', num2str(estErr)  , ' meters.']);
end

% Draw the cost function table
% Not implemented correctly yet. Assigns the cost function to the workspace
% instead.

if tableON
    ynames = xGrid(1,:);
    xnames = yGrid(:,1);    
    assignin('base', 'ynames', ynames);
    assignin('base', 'xnames', xnames);
    assignin('base', 'theta', theta);
    assignin('base', 'sDist', sDist);
    disp('To see the table open ''theta'' variable in the workspace.')
    disp(['Emitter is minimum at the indices ['...
        , num2str(Iy), ','...
        , num2str(Ix), '] of the theta variable.']);
end

% Plot the resulting cost function
if(plotON~=0)    
    % We will use this figure to plot the cost function 3D
    figure('name','Emitter Location Estimation','numbertitle','off');

    % Find how many subplots are neccessary
    plotSize = 0;
    while(abs(plotON)>plotSize^2)
        plotSize=plotSize+1;
    end

    h=zeros(1,plotSize);
    for i=1:abs(plotON)        
        if (exist('subtightplot', 'file'))
            subtightplot(plotSize,plotSize,i...
                            ,[0.1 0.1], [0.1 0.1], [0.1 0.1]);
        else
            subplot(plotSize,plotSize,i)
        end
        % Change large values to NaN for better visual
        visual = theta;           
        if(plotON>0)
            for j=1:i-1
                visual(visual>=(max(visual(:))-min(visual(:)))/2) = NaN;
            end
        end
        if(plotON<0)
            visual(visual>=nanmean(visual(:))) = NaN;
            for j=1:i
                visual(visual>=nanmean(visual(:))) = NaN;
            end
        end       
        colormap([jet(128);gray(128)])
        h(i) = surf(xGrid,yGrid,visual);

        % Initially, both CDatas are equal to cost.
        color_S = 128; % 128-elements is each colormap

        % CData for surface
        cmin = min(visual(:));
        cmax = max(visual(:));
        C1 = min(color_S,round((color_S-1)*(visual-cmin)/(cmax-cmin))+1); 

        % CData for pcolor
        C2 = 128+C1;

        % Update the CDatas for each object.
        set(h(i),'CData',C1);

        % Change the CLim property of axes so that it spans the 
        % CDatas of both objects.
        caxis([min(C1(:)) max(C2(:))])

        set(h(i),'EdgeColor','none');       % This setting will make sure 
                                            % that the plot is not black 
                                            % because of default wire mesh

        % Set labels and title
        title(['Vertical Zoom Level ', num2str(i)]);
        xlabel('x');
        ylabel('y');
        zlabel('Cost/Objective Function');


        % Mark minimum on the map       
        try
            makedatatip(h(i),[Iy Ix]);
        catch            
            disp('Minimum value not shown as makedatatip.m is not found.');
        end
    end

    % Enlarge figure
    set(gcf, 'units','normalized','outerposition',[0.05 0.13 0.9 0.8]);  
end

end

function [] = plotSELocations(ROI, gridSize, sPos, ePos, estELoc)
%% PLOTSELOCATIONS plots sensors and emittors on figure to show locations
% Required inputs are;
% - Region of interest axis length (ROI)
% - Grid axis length (gridSize)
% - Sensor positions given as [x;y] (sPos)
% - Emitter positions given as [x;y] (ePos)

figure();

% Create whole area
axis([0 ROI+1 0 ROI+1]);
hold on;

% Place actual sensors
scatter(sPos(1,:),sPos(2,:), 70, 'x');

% Place actual emitters also determine color for each
c(1,:)= [round(rand), round(rand), round(rand)];
while isequal(c(1,:),[1 1 1])
    c(1,:)= [round(rand), round(rand), round(rand)];
end

scatter(ePos(1,1),ePos(2,1), 70, 's' ...
            , 'MarkerFaceColor', c(1,:) ...
            , 'MarkerEdgeColor', c(1,:));

% Place estimated emitter locations
scatter(estELoc(1,1), estELoc(2,1), 70, 'd'...
            , 'MarkerFaceColor', c(1,:)...
            , 'MarkerEdgeColor', c(1,:));

hold off;
% Set grid on with the given size
tickValues = 0:gridSize:ROI+1;
set(gca,'YTick',tickValues)
set(gca,'XTick',tickValues)
grid on;

% Set title, labels and legend
title('Sensor and Emitter placements for FGS_{SE}');
xlabel('x');
ylabel('y');
legend('Sensors', 'Emitter', 'Estimated Emitter');

end